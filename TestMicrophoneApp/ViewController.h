//
//  ViewController.h
//  TestMicrophoneApp
//
//  Created by Martin Christmann on 11/8/13.
//  Copyright (c) 2013 Martin Christmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>

@interface ViewController : UIViewController
@property AVAudioRecorder *recorder;
@property NSTimer *levelTimer;
@property double lowPassResults;


- (void)levelTimerCallback:(NSTimer *)timer;

@end
